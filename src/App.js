import React, { Component } from 'react';
import mobiles from './data'
import './App.css';
import Search from './components/Search'
import MobileList from './components/MobileList'

class App extends Component {
  state = {...mobiles, filterString : ''}

  updateFilterString =(value)=>{
      console.log(value)
      this.setState({filterString : value})
  }
  render() {
    console.log(this.state.mobiles)
    return (
      <div className="App">
        <Search onDoSearch = {this.updateFilterString}/>
        <MobileList {...this.state}/>
      </div>
    );
  }
}

export default App;

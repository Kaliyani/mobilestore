export default {
    "mobiles":[
      {
        "id": 1,
        "brand": "Sony",
        "model":"X1",
        "price": 20
      },
      {
        "id": 2,
        "brand": "Moto",
        "model":"G",
        "price": 30
      },
      {
        "id": 3,
        "brand": "Samsung",
        "model":"mini",
        "price": 50
      },
      {
        "id": 4,
        "brand": "Nokia",
        "model":"1100",
        "price": 60
      }
    ]
}
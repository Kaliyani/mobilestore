import React, { Component } from 'react'

export default class Search extends Component {
state = { searchStr:''}

onChangeInput = (e)=>{
    this.setState({searchStr : e.target.value})
}

onSearchClick =()=>{
    this.props.onDoSearch(this.state.searchStr)
}
  render() {
    return (
      <div>
        <input onChange={this.onChangeInput}/>
        <button onClick={this.onSearchClick}>Search</button>
      </div>
    )
  }
}

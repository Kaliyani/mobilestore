import React, { Component } from 'react'

export default class MobileListItem extends Component {
    
  render() {
    const {brand, model, price} = this.props.mobile
    return (
      <div>
          <div>{brand}</div>
          <div>{model}</div>
          <div>{price}</div> 
      </div>
    )
  }
}

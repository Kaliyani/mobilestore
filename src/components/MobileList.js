import React, { Component } from 'react'
import MobileListItem from './MobileListItem'

export default class MobileList extends Component {
  render() {
        const {mobiles, filterString} = this.props
        const filteredMobiles = mobiles.filter( (mobile) => {
            return mobile.brand && (mobile.brand.toLowerCase()).indexOf(filterString.toLowerCase()) !== -1
        })
    return (
      <div>
        {
            filteredMobiles.map( (filterMobile) =>{
                return <MobileListItem mobile = {filterMobile}/>
                
            })
        }
      </div>
    )
  }
}
